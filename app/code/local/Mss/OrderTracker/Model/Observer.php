<?php 

class Mss_OrderTracker_Model_Observer
{
	const XML_SETTING_ORDER_TRACKING_STATUS_ID = 'mssconfig/mss_group/mss_enable';


    public function OrderTrackerConfiguration( Varien_Event_Observer $observer) {

    	$tracking_status = Mage::getStoreConfig(self::XML_SETTING_ORDER_TRACKING_STATUS_ID);
    	if($tracking_status){
	    $couponContainer = $observer->getEvent()->getCouponContainer();
	    $couponContainer->setOrderTracker($tracking_status);
		} 
    }
}
?>