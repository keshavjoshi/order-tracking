<?php
class Mss_OrderTracker_Helper_Data extends Mage_Core_Helper_Abstract
{
	const XML_CONFIG_PATH = "mssconfig/settings/";

	public function trackingUrl()
    {
        return Mage::getUrl('ordertracker/tracking/index');
    }

    public function resultUrl()
    {
		return Mage::getUrl('ordertracker/tracking/ordertrack');
	}

	public function getIsEnabled()
	{
		return Mage::getStoreConfigFlag('mssconfig/mss_group/mss_enable');
	}

	 public function forgotorderUrl()
    {
        return Mage::getUrl('ordertracker/tracking/forgotajax');
    }
}
