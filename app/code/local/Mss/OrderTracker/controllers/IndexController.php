<?php
class Mss_OrderTracker_IndexController extends Mage_Core_Controller_Front_Action
{
	public function _construct() 
    {

        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        Mage::helper('connector')->loadParent(Mage::app()
                                 ->getFrontController()
                                 ->getRequest()
                                 ->getHeader('token'));
        parent::_construct(); 
    }

	/*
	*	URL : baseurl/restapi/ordertracker/orderTrackerDetail/
	*	Name : orderTrackerDetail
	*	Method : GET
	*	Parameters : order_number , order_zipcode , order_email , order_phonenumber
	*	Response : JSON
	*	Detail : This Api use for tracking order status
	*/	
		
	public function orderTrackerDetailAction() {
     
        $order_number = $this->getRequest->getParam('order_number');
        $order_zipcode = $this->getRequest->getParam('order_zipcode');
        $order_email = $this->getRequest->getParam('order_email');
        $order_phonenumber = $this->getRequest->getParam('order_phonenumber');
        $response = array();
        $orderId =  Mage::getModel('sales/order')->loadByIncrementId($order_number)->getEntityId(); 
        $order = Mage::getModel("sales/order")->load($orderId);
        $fieldmultiselect = Mage::getStoreConfig('mssconfig/mss_group/mss_specific_field');
        $fieldmultiselect = explode(",",$fieldmultiselect);
        $getOrderData = $order->getData();
        if(!empty($getOrderData)){ 
            $cEmail = $order->getCustomerEmail();
            $billing = $order->getBillingAddress();
            $shipping = $order->getShippingAddress(); 
            $zipcode = $billing->getPostcode();
            $phonenumber = $billing->getTelephone();
            $response['image']           = '';
            $response['product name']    = '';            
            $response['orderdetail_Url'] = '';
            $response['billing_info']    = '';
            $response['shipping_info']   = '';
            $response['subtotal']        = '';
            $response['shipping']        = '';
            $response['tax']             = '';
            $response['discount']        = '';
            $response['grandtotal']      = '';
            $response['order_number']    = '';
            $response['order_email']     = '';
            $response['status']          = '';
            $response['shipped_dates']   = '';
            $response['billed_on']       = '';
            if(in_array(2,$fieldmultiselect) && in_array(3,$fieldmultiselect)){
                $condition = ($zipcode == trim($order_zipcode) && $phonenumber === trim($order_phonenumber));
            }elseif(in_array(2,$fieldmultiselect) && in_array(1,$fieldmultiselect)){
                 $condition = ($zipcode == trim($order_zipcode) && $cEmail == trim($order_email));
            }elseif(in_array(2,$fieldmultiselect) && in_array(1,$fieldmultiselect) && in_array(3,$fieldmultiselect)){
                 $condition = ($cEmail ==  trim($order_email) && $zipcode == trim($order_zipcode) && $phonenumber === trim($order_phonenumber));
            }elseif(in_array(3,$fieldmultiselect) && in_array(1,$fieldmultiselect)){
                 $condition = ($phonenumber === trim($order_phonenumber) && $cEmail == trim($order_email));
            }elseif(in_array(1,$fieldmultiselect) || in_array(2,$fieldmultiselect) || in_array(3,$fieldmultiselect)){
                $condition = ($cEmail == trim($order_email) || $zipcode == trim($order_zipcode) || $phonenumber === trim($order_phonenumber));
            }else{
                $condition = ($order_number != '');
            }
            $block = Mage::app()->getLayout()->createBlock('sales/order_item_renderer_default');
            if($condition != '') {
                $orderVisibleItem = $order->getAllVisibleItems();
                foreach($orderVisibleItem as $item)
                { 
                    $item->getSku();
                    $_product = Mage::getModel('catalog/product')->load($item->getProductId());
                    $response['image'] = $_product->getSmallImageUrl(); 
                    $response['product name'] =  $item->getName();
                    $block->setItem($item);     
                    if ($options = $block->getItemOptions()){
                        $j=0;
                        foreach($options as $option){
                            $label[$i] = $option['label'];
                            $textvalue[$j] = $option['value'];
                            $response['orderdetail_Url'] = $label[$i].':'.$textvalue[$j];
                            $j++;
                        }
                    }
                    $response['Price']     = number_format($item->getPrice(),2);
                    $response['Quantity']  = number_format($item->getQtyOrdered(),2);
                }
                $grandTotal                = $order['grand_total'];
                $orderSubtotal             = $order->getSubtotal();
                $ordershippingAmount       = $order->getShippingAmount();
                $orderTax                  = $order->getTaxAmount();
                $orderDiscount             = $order->getDiscountAmount();
                $orderStatus               = $order->getStatus();
                $response['billing_info']  = $billing->getData();
                $response['shipping_info'] = $shipping->getData();
                $response['subtotal']      = number_format($orderSubtotal,2);
                $response['shipping']      = number_format($ordershippingAmount,2);
                $response['tax']           = number_format($orderTax,2);
                $response['discount']      = number_format($orderDiscount,2);
                $response['grandtotal']    = number_format($grandTotal,2);
                $response['order_number']  = $order_number;
                $response['order_email']   = $order_email;
                $response['status']        = $orderStatus;
                $shipping_method           = $order->getShippingDescription();
                $payment                   = $order->getPayment()->getMethodInstance()->getTitle();
                $response['payment']       = $payment;
                $response['carriers']      = $shipping_method;
                $orderShipmentCollection   = $order->getShipmentsCollection();
                $orderCreatedAt            = $order->getCreatedAt();
                foreach($orderShipmentCollection as $shipment)
                {
                    $shipmentCreatedAt = $shipment->getCreatedAt();
                    $response['shipped_dates'] =  date("M d, Y h:i:s A",strtotime($shipment->getCreatedAt()));
                }
                $response['billed_on'] = date("M d, Y h:i:s A",strtotime($orderCreatedAt));
            }else{
                $errormsg = 'Entered data is incorrect.Please try again.';
                $response['error_status'] = $errormsg;
            }
        }else{
            $errormsg = 'Entered data is incorrect.Please try again.';
            $response['error_status'] = $errormsg;
        }
        if($order_number == ''){
            $response['order_number'] =  $order_number;
            $response['error_status'] = 'Please fill the required field';
        }
        echo json_encode($response);
	}
}
?>