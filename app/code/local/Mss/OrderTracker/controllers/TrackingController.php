<?php
class Mss_OrderTracker_TrackingController extends Mage_Core_Controller_Front_Action
{
	const XML_PATH_FORGOT_ORDERNUMER_EMAIL = 'mssconfig/mss_group/custom_template';
	const XML_PATH_FORGOT_ORDERNUMER_EMAIL_IDENTITY = 'mssconfig/mss_group/custom_identity';
	
	public function indexAction()
	{  
		$this->loadLayout();
		$this->renderLayout();  
	} 

	public function ajaxAction()
	{
		$params = $this->getRequest()->getParams();
		$response = array();
		$orderId =  Mage::getModel('sales/order')->loadByIncrementId($params['order_number'])->getEntityId(); 
		$order = Mage::getModel("sales/order")->load($orderId);
		$fieldmultiselect = Mage::getStoreConfig('mssconfig/mss_group/mss_specific_field');
		$fieldmultiselect = explode(",",$fieldmultiselect);
		$getOrderData = $order->getData();
		if(!empty($getOrderData)){ 
			$cEmail = $order->getCustomerEmail();
			$billing = $order->getBillingAddress();
			$shipping = $order->getShippingAddress(); 
			$zipcode = $billing->getPostcode();
			$phonenumber = $billing->getTelephone();
			$response['orderdetail_header'] = '';
			$response['orderdetail_html'] = '';
			$response['billing_info'] = '';
			$response['shipping_info'] = '';
			$response['subtotal'] = '';
			$response['shipping'] = '';
			$response['tax'] = '';
			$response['discount'] = '';
			$response['grandtotal'] = '';
			$response['order_number'] = '';
			$response['order_email'] = '';
			$response['status'] = '';
			$response['shipped_dates'] = '';
			$response['billed_on'] = '';
			$fieldTodisplayemail = Mage::getStoreConfig('mssconfig/mss_group/mss_email');
			$fieldTodisplayzipcode = Mage::getStoreConfig('mssconfig/mss_group/mss_zipcode');
			$fieldTodisplayphonenumber = Mage::getStoreConfig('mssconfig/mss_group/mss_phonenumber');
			
			if(in_array(2,$fieldmultiselect) && in_array(3,$fieldmultiselect)){
				$condition = ($zipcode == trim($params['order_zipcode']) && $phonenumber === trim($params['order_phonenumber']));
			}elseif(in_array(2,$fieldmultiselect) && in_array(1,$fieldmultiselect)){
				 $condition = ($zipcode == trim($params['order_zipcode']) && $cEmail == trim($params['order_email']));
			}elseif(in_array(2,$fieldmultiselect) && in_array(1,$fieldmultiselect) && in_array(3,$fieldmultiselect)){
				 $condition = ($cEmail ==  trim($params['order_email']) && $zipcode == trim($params['order_zipcode']) && $phonenumber === trim($params['order_phonenumber']));
			}elseif(in_array(3,$fieldmultiselect) && in_array(1,$fieldmultiselect)){
				 $condition = ($phonenumber === trim($params['order_phonenumber']) && $cEmail == trim($params['order_email']));
			}elseif(in_array(1,$fieldmultiselect) || in_array(2,$fieldmultiselect) || in_array(3,$fieldmultiselect)){
				$condition = ($cEmail == trim($params['order_email']) || $zipcode == trim($params['order_zipcode']) || $phonenumber === trim($params['order_phonenumber']));
			}else{
				$condition = ($params['order_number'] != '');
			}
			$block = Mage::app()->getLayout()->createBlock('sales/order_item_renderer_default');
			if($condition != '') {
				$response['orderdetail_header'] .='<tr><th>Items</th>';
				
				$orderVisibleItem = $order->getAllVisibleItems();
				foreach($orderVisibleItem as $item)
				{ 
					$item->getSku();
					$_product = Mage::getModel('catalog/product')->load($item->getProductId());
					$response['orderdetail_html'] .= '<tr><td><img width="55" height="55" src="'.$_product->getSmallImageUrl().'" class="dataImage"><h3 class="dataName">'.$item->getName().'</h3>';
					$block->setItem($item);		
					if ($options = $block->getItemOptions()){
						$j=0;
						foreach($options as $option){
							$label[$i] = $option['label'];
							$textvalue[$j] = $option['value'];
							$response['orderdetail_html'] .= '<p class="dataStyle">style #:'.$label[$i].':'.$textvalue[$j].'</p>';
							$j++;
						}
					}
					$response['orderdetail_html'] .='</td><td><span><span class="price">'.number_format($item->getPrice(),2).'</span></span></td><td><span>'.number_format($item->getQtyOrdered(),2).'</span></td><td class="dataTotal"><span><span class="price">'.number_format($item->getPrice(),2).'</span></span></td></tr>';
				}
				$grandTotal = $order['grand_total'];
				$orderSubtotal = $order->getSubtotal();
				$ordershippingAmount = $order->getShippingAmount();
				$orderTax = $order->getTaxAmount();
				$orderDiscount = $order->getDiscountAmount();
				$orderStatus = $order->getStatus();
				$response['orderdetail_header'] .='<th>Price</th><th>Quantity</th><th class="dataTotal">Total</th></tr>';
				$response['billing_info'] = $billing->format('html');
				$response['shipping_info'] = $shipping->format('html');
				$response['subtotal'] = '<span class="price">'.number_format($orderSubtotal,2).'</span>';
				$response['shipping'] = '<span class="price">'.number_format($ordershippingAmount,2).'</span>';
				$response['tax'] = '<span class="price">'.number_format($orderTax,2).'</span>';
				$response['discount'] = '<span class="price">'.number_format($orderDiscount,2).'</span>';
				$response['grandtotal'] = '<span class="price">'.number_format($grandTotal,2).'</span>';
				$response['order_number'] =$params['order_number'];
				$response['order_email'] = $params['order_email'];
				$response['status'] = $orderStatus;
				$shipping_method = $order->getShippingDescription();
				$payment =  $order->getPayment()->getMethodInstance()->getTitle();
				$response['payment'] = $payment;
				$response['carriers'] = $shipping_method;
				
				$orderShipmentCollection = $order->getShipmentsCollection();
				$orderCreatedAt = $order->getCreatedAt();
				foreach($orderShipmentCollection as $shipment)
				{
					$shipmentCreatedAt = $shipment->getCreatedAt();
					$response['shipped_dates'] =  date("M d, Y h:i:s A",strtotime($shipment->getCreatedAt()));
				}
				$response['billed_on'] = date("M d, Y h:i:s A",strtotime($orderCreatedAt));
					
			}else{
				$errormsg = 'Entered data is incorrect.Please try again.';
				$response['error_status'] = $errormsg;
			}
			
		}else{
			$errormsg = 'Entered data is incorrect.Please try again.';
			$response['error_status'] = $errormsg;
		}
		
		if($params['order_number'] == ''){
			$response['order_number'] =$params['order_number'];
			$response['error_status'] = 'Please fill the required field';
		}
		
		$this->getResponse()->setBody(Mage::Helper('core')->jsonEncode($response));
	}

	public function ordertrackAction()
	{ 
		$params = $this->getRequest()->getParams();
		$response = array();
		$orderId =  Mage::getModel('sales/order')->loadByIncrementId($params['order_number'])->getEntityId(); 
		$order = Mage::getModel("sales/order")->load($orderId);
		$fieldmultiselect = Mage::getStoreConfig('mssconfig/mss_group/mss_specific_field');
		$fieldmultiselect = explode(",",$fieldmultiselect);
		$getOrderData = $order->getData();
		if(empty($getOrderData))
		{
			$errormsg = 'Entered data is incorrect.Please try again.';
			$response['error_status'] = $errormsg;
		}
		if(!empty($getOrderData)){ 
			$cEmail = $order->getCustomerEmail();
			$billing = $order->getBillingAddress();
			$zipcode = $billing->getPostcode();
			$phonenumber = $billing->getTelephone();
			$response['orderdetail_header'] = '';
			$response['orderdetail_html'] = '';
			$response['billing_info'] = '';
			$response['shipping_info'] = '';
			$response['subtotal'] = '';
			$response['shipping'] = '';
			$response['tax'] = '';
			$response['discount'] = '';
			$response['grandtotal'] = '';
			$response['order_number'] = '';
			$response['order_email'] = '';
			$response['status'] = '';
			$response['shipped_dates'] = '';
			$response['billed_on'] = '';
			$shipping_method = $order->getShippingDescription();
				$response['carriers'] = $shipping_method;
			if(in_array(2,$fieldmultiselect) && in_array(3,$fieldmultiselect)){
				$condition = ($zipcode == trim($params['order_zipcode']) && $phonenumber === trim($params['order_phonenumber']));
			}elseif(in_array(2,$fieldmultiselect) && in_array(1,$fieldmultiselect)){
				 $condition = ($zipcode == trim($params['order_zipcode']) && $cEmail == trim($params['order_email']));
			}elseif(in_array(2,$fieldmultiselect) && in_array(1,$fieldmultiselect) && in_array(3,$fieldmultiselect)){
				 $condition = ($cEmail ==  trim($params['order_email']) && $zipcode == trim($params['order_zipcode']) && $phonenumber === trim($params['order_phonenumber']));
			}elseif(in_array(3,$fieldmultiselect) && in_array(1,$fieldmultiselect)){
				 $condition = ($phonenumber === trim($params['order_phonenumber']) && $cEmail == trim($params['order_email']));
			}elseif(in_array(1,$fieldmultiselect) || in_array(2,$fieldmultiselect) || in_array(3,$fieldmultiselect)){
				$condition = ($cEmail == trim($params['order_email']) || $zipcode == trim($params['order_zipcode']) || $phonenumber === trim($params['order_phonenumber']));
			}
			$block = Mage::app()->getLayout()->createBlock('sales/order_item_renderer_default');
			if ($condition != '') {
				$ship_flag = '';
				$response['orderdetail_header'] .='<tr><th>Items</th>';
				$orderVisibleItem = $order->getAllVisibleItems();
				foreach($orderVisibleItem as $item)
				{ 
					$item->getSku();
					$_product = Mage::getModel('catalog/product')->load($item->getProductId());
					$response['orderdetail_html'] .= '<tr><td><img width="55" height="55" src="'.$_product->getSmallImageUrl().'" class="dataImage"><h3 class="dataName">'.$item->getName().'</h3>';
					$block->setItem($item);	
					$i = 0;	
					if ($options = $block->getItemOptions()){
						$j=0;
						foreach($options as $option){
							$label[$i] = $option['label'];
							$textvalue[$j] = $option['value'];
							$response['orderdetail_html'] .= '<p class="dataStyle">style #:'.$label[$i].':'.$textvalue[$j].'</p>';
							$j++;
						}
					}
					$response['orderdetail_html'] .='</td><td><span><span class="price">'.number_format($item->getPrice(),2).'</span></span></td><td><span>'.number_format($item->getQtyOrdered(),2).'</span></td><td class="dataTotal"><span><span class="price">'.number_format($item->getPrice(),2).'</span></span></td></tr>';
				}
				$response['orderdetail_header'] .='<th>Price</th><th>Quantity</th><th class="dataTotal">Total</th></tr>';
				$getShippingAddress = $order->getShippingAddress();
				$getBillingAddress = $order->getBillingAddress();
				if (empty($getShippingAddress)) {
					 $ship_flag = 0;
				} else {
					 $ship_flag = 1;
					 $shipping = $order->getShippingAddress()->format('html'); 
				}
				if (empty($getBillingAddress)) {
					 $bill_flag = 0;
				} else {
					 $bill_flag = 1;
					 $billing = $order->getBillingAddress()->format('html');	 
				}
				$response['billing_info'] = $billing;
				$response['shipping_info'] = $shipping;
				$orderSubtotal = $order->getSubtotal();
				$ordershippingAmount = $order->getShippingAmount();
				$orderTax = $order->getTaxAmount();
				$orderDiscount = $order->getDiscountAmount();
				$orderStatus = $order->getStatus();
				$response['subtotal'] = '<span class="price">'.number_format($orderSubtotal,2).'</span>';
				$response['shipping'] = '<span class="price">'.number_format($ordershippingAmount,2).'</span>';
				$response['tax'] = '<span class="price">'.number_format($orderTax,2).'</span>';
				$response['discount'] = '<span class="price">'.number_format($orderDiscount,2).'</span>';
				$grandTotal = $order['grand_total'];;
				$response['grandtotal'] = '<span class="price">'.number_format($grandTotal,2).'</span>';
				$response['order_number'] = $params['order_number'];
				$response['order_email'] = $params['order_email'];
				$response['status'] = $orderStatus;
				$shipping_method = $order->getShippingDescription();
				$response['carriers'] = $shipping_method;
				
				$orderShipmentCollection = $order->getShipmentsCollection();
				$orderCreatedAt = $order->getCreatedAt();
				
				foreach($orderShipmentCollection as $shipment)
				{
					$shipmentCreatedAt = $shipment->getCreatedAt();
					$response['shipped_dates'] = date("M d, Y h:i:s A",strtotime($shipmentCreatedAt));
				}
				$response['billed_on'] = date("M d, Y h:i:s A",strtotime($orderCreatedAt));
				
				$this->loadLayout();
				$this->getLayout()
					 ->getBlock('ordertracker.tracking.track')
					 ->setTemplate('mss/ordertracker/track.phtml');
				$block = $this->getLayout()
							  ->getBlock('ordertracker.tracking.track');
				$block->setData('order_number',$params['order_number']);
				$block->setData('billing_info',$response['billing_info']);
				$block->setData('shipping_info',$response['shipping_info']);
				$block->setData('subtotal',$response['subtotal']);
				$block->setData('shipping',$response['shipping']);
				$block->setData('tax',$response['tax']);
				$block->setData('discount',$response['discount']);
				$block->setData('grandtotal',$response['grandtotal']);
				$block->setData('status',$response['status']);
				$block->setData('billed_on',$response['billed_on']);
				$block->setData('carriers',$response['carriers']);
				$block->setData('shipped_dates',$response['shipped_dates']);
				$block->setData('orderdetail_header',$response['orderdetail_header']);
				$block->setData('orderdetail_html',$response['orderdetail_html']);
				$block->setData('order_number',$params['order_number']);
				$payment =  $order->getPayment()->getMethodInstance()->getTitle();
				$block->setData('payment',$payment);
				$this->renderLayout();
				
			}else{
				$this->loadLayout();
				$this->getLayout()
					 ->getBlock('ordertracker.tracking.track')
					 ->setTemplate('mss/ordertracker/track.phtml');
				$block = $this->getLayout()
							  ->getBlock('ordertracker.tracking.track');
				$errormsg = 'Entered data is incorrect.Please try again.';
				$block->setData('error_status',$errormsg);
				$this->renderLayout();
			}
		}else{
				$this->loadLayout();
				$this->getLayout()
					 ->getBlock('ordertracker.tracking.track')
					 ->setTemplate('mss/ordertracker/track.phtml');
				$block = $this->getLayout()
							  ->getBlock('ordertracker.tracking.track');
				$errormsg = 'Entered data is incorrect.Please try again.';
				$block->setData('error_status',$errormsg);
				$this->renderLayout();
		}
	}

	 public function forgotajaxAction()
	 {
		$params = $this->getRequest()->getParams(); 
		$params['order_email']; 
		$response = array();
		if($params['order_email']!= ''){
			$response['order_email'] = $params['order_email'];
			$ordercustomername = Mage::getResourceModel('sales/order_collection')
									->addFieldToSelect('customer_firstname')
									->addFieldToSelect('customer_lastname')
									->addFieldToFilter('customer_email',$response['order_email'])
									->getFirstItem();
			if(count($ordercustomername)>0){
				$customername = $ordercustomername->getCustomerFirstname().' '.$ordercustomername->getCustomerLastname();
			}
			$ordertrack = Mage::getModel('ordertracker/ordertrack')->getCollection()
							->addFieldToSelect('ordertrack_id')
							->addFieldToSelect('emailtime')
							->addFieldToSelect('email')
							->addFieldToFilter('email',$response['order_email'])
							->getFirstItem();
			if($ordertrack['emailtime'] !='' && $ordertrack['email'] == $response['order_email']){ 
				$date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
				$dateA = $ordertrack['emailtime']; 
				$dateB = $date; 
				$timediff = strtotime($dateB) - strtotime($dateA);
				if($timediff >= 86400){ 
					$translate = Mage::getSingleton('core/translate');
					$translate->setTranslateInline(false);
					$storeId = Mage::app()->getStore()->getId();
					$mailTemplate = Mage::getModel('core/email_template');
					$mailTemplate->setDesignConfig(array('area'=>'frontend'))
								 ->sendTransactional(
									  Mage::getStoreConfig(self::XML_PATH_FORGOT_ORDERNUMER_EMAIL),
									  Mage::getStoreConfig(self::XML_PATH_FORGOT_ORDERNUMER_EMAIL_IDENTITY),
									  $response['order_email'],
									  array(
										'name'  => ucwords($customername),
										'email' => $response['order_email'],
									  )
								 ); 
					$translate->setTranslateInline(true);
					$ordertrack = Mage::getModel('ordertracker/ordertrack')->load($ordertrack['ordertrack_id']);
					$date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
					$data=array(
							'emailtime'=>$date,
							'email'=>$response['order_email'],
							);
					$ordertrack->setEmailtime($date);
					$ordertrack->save();
					$response['error_status'] = 'Order Numer with respect to email address is sent to your email address.';
				}
				else
				{
					$response['error_status'] = 'Only one email can be send within 24hrs';
				}	
			}else{  
				$ordercollection = Mage::getResourceModel('sales/order_collection')
									->addFieldToSelect('*')
									->addFieldToFilter('customer_email',$response['order_email']);
				$orderCollectionData = $ordercollection->getData();
				if(!empty($orderCollectionData))
				{
					$translate = Mage::getSingleton('core/translate');
					$translate->setTranslateInline(false);
					$storeId = Mage::app()->getStore()->getId();
					$mailTemplate = Mage::getModel('core/email_template');
					$mailTemplate->setDesignConfig(array('area'=>'frontend'))
								 ->sendTransactional(
									  Mage::getStoreConfig(self::XML_PATH_FORGOT_ORDERNUMER_EMAIL),
									  Mage::getStoreConfig(self::XML_PATH_FORGOT_ORDERNUMER_EMAIL_IDENTITY),
									  $response['order_email'],
									  array(
										'name'  => ucwords($customername),
										'email' => $response['order_email'],
									  )
								 ); 
					$translate->setTranslateInline(true);
					$date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
					$data=array(
							'emailtime'=>$date,
							'email'=>$response['order_email'],
							);
					$model=Mage::getModel('ordertracker/ordertrack')->addData($data);
					$model->save();
					$response['error_status'] = 'Order Numer with respect to email address is sent to your email address.';
				}else{
					$response['error_status'] = 'No order place with this email address';	
				}				
			}				
		}
		$this->getResponse()->setBody(Mage::Helper('core')->jsonEncode($response));
	 }
}
