<?php
class Mss_OrderTracker_Block_Tracking extends Mage_Core_Block_Template{

	public function getFormUrl()
	{
		return $this->helper('ordertracker')->resultUrl();
	}

	public function ordertrackUrl()
	{
		return $this->helper('ordertracker')->trackingUrl();
	}

	 public function getForgotorderurl(){
		 return $this->helper('ordertracker')->forgotorderUrl();
	 }
	
}
